from __future__ import annotations

from itertools import chain, combinations_with_replacement, product
from collections.abc import Iterable
from functools import lru_cache
from pickle import loads
from typing import Union
from sympy import S,Expr
from enum import Enum

from sympy.core.evalf import N



from DatabaseInterface import DBInterface
from DatabaseInterface import ReferenceTable,RefValTable

from CoefficientSupport import PackExpr, PairConf, RadialType, UnpackExpr
from CoefficientSupport import KinType,sFConf,FConf
from CoefficientSupport import KD,symAvg,uMax

from CoefficientSupport import sD_C
from CoefficientSupport import sF_C,sF_Kin_C
from CoefficientSupport import G_pre

from CoefficientSupport import sTuple,sF_IdxTuple
from CoefficientSupport import F_IdxTuple
from CoefficientSupport import G_IdxTuple,HI_IdxTuple,J_IdxTuple
from CoefficientSupport import g_IdxTuple,hi_IdxTuple,j_IdxTuple




# === Single state cofficients =================================================

class sD(RefValTable):
    def __init__(self,DB:DBInterface):

        super().__init__(DB,'sD','m,l,k','s','sD,sD_n','mlk')

    @staticmethod
    def KeyIdxIter(Keys):
        for key in Keys:
            (m,l,_) = key
            for s in sTuple(l-m,KinType.Kin):
                yield (key,(s,))

    @classmethod
    def MapVal(cls,key,idx):
        (m,l,k) = key
        (s,) = idx
        res = sD_C(m,l,k,s)
        return cls.PackValues(res)

    @staticmethod
    def PackValues(Value:Expr):
        return PackExpr(Value)

    @staticmethod
    def UnpackValues(pValue:tuple[bytes,float]):
        return UnpackExpr(pValue)









# ==============================================================================

class CoeffTable(RefValTable):
    val_t = Expr
    pval_t = tuple[bytes,float]

    # General coefficient table accessor class
    def __init__(self,
        DB:DBInterface,
        DestName:str,
        KeyLabels:str,IdxLabels:str,ValLabels:str,
        RefName:str,
        **kwargs,
    ):

        try:
            PartName = kwargs["PartName"]
            PartLabels = kwargs["PartLabels"]
            self.PartTable = ReferenceTable(DB,PartName,PartLabels)
        except KeyError:
            PartName = None
            PartLabels = None
            self.PartTable = None
        try:
            ConfInst = kwargs["ConfInst"]
        except KeyError:
            ConfInst = None
        try:
            self.SourceTables:dict[Enum,CoeffTable] = kwargs["SourceTables"]
        except KeyError:
            self.SourceTables = None

        super().__init__(
            DB,DestName,KeyLabels,IdxLabels,ValLabels,RefName,ConfInst
        )

        self.SourceConf = None

    def Insert(self,KeyParts:Iterable):
        KeyParts = tuple(KeyParts)
        if self.SourceTables is not None:
            self.SourceInsert(KeyParts)
        super().Insert(KeyParts)

    def SourceInsert(self,Keys:Iterable):
        self.SourceTables[self.SourceConf].Insert(Keys)

    def GenerateKeys(self,KeyParts:Iterable):
        if self.PartTable is not None:
            ExistingKeyParts = tuple(self.PartTable._GetRows())
            NewKeyParts = tuple(
                x
                for x in KeyParts
                if x not in ExistingKeyParts
            )
            RawKeys = tuple(chain(
                combinations_with_replacement(ExistingKeyParts,2),
                product(ExistingKeyParts,NewKeyParts),
                combinations_with_replacement(NewKeyParts,2),
            ))
            FoundKeys = self.CheckKeys(tuple( x[1]+x[0] for x in RawKeys ))
            return tuple( 
                x[1]+x[0] if x[1]+x[0] in FoundKeys else x[0]+x[1]
                for x in RawKeys
            )
        else:
            return super().GenerateKeys(KeyParts)


    @staticmethod
    def PackValues(Value:val_t) -> pval_t:
        return PackExpr(Value)

    @staticmethod
    def UnpackValues(pValue:pval_t) -> val_t:
        return UnpackExpr(pValue)




class sFTable(CoeffTable):
    _KeyType = tuple[int,int]
    _IdxType = tuple[int,int,int]
    _AccessTupleType = tuple[int,int,int,int]
    _AccessType = Iterable[tuple[int,Expr]]

    def __init__(self,DB:DBInterface,ConfInst:sFConf):
        TableName = 'sF'
        ReferenceName = 'lk'
        KeyLabels = 'l,k'
        IdxLabels = 's,U,x'
        ValueLabels = 'sF,sF_n'

        super().__init__(
            DB,
            TableName,KeyLabels,IdxLabels,ValueLabels,
            ReferenceName,
            ConfInst=ConfInst
        )
        self.ConfInst:sFConf

    def _MakeIndex(self):

        super()._MakeIndex()

        subs = {
            'Named':'sFAccess'+'_'+self.ConfInst.name,
            'Table':self.TableName,
            'Cols':'l,k,s,U,x,sF'
        }
        comm = (
            "SELECT * FROM sqlite_schema WHERE type='index'"
            +" and name='{Named}'"
        ).format(**subs)
        if self.DB.Execute(comm).fetchone() is None:
            comm = (
                'CREATE INDEX IF NOT EXISTS {Named} ON {Table} ({Cols})'
            ).format(**subs)
            self.DB.Execute(comm)
            self.DB.Commit()


    @lru_cache
    def Access(self,tup:_AccessTupleType) -> _AccessType:
        return tuple(
            (x,S(loads(sF)))
            for (x,sF) in
            self._MatchRows('x,sF','l,k,s,U',(tup,))
        )

    def IdxTuple(self,key:_KeyType):
        return sF_IdxTuple(key,self.ConfInst.KinType)

    def MapVal(self,key:_KeyType,idx:_IdxType):
        (l,k,) = key
        (s,U,x,) = idx
        Wf = self.ConfInst.Wf
        if ( self.ConfInst.KinValue ):
            res = sF_Kin_C(Wf,l,k,s,U,x)
        else:
            res = sF_C(Wf,l,k,U,x)
        return self.PackValues(res)



class FTable(CoeffTable):
    _KeyPartType = tuple[int,int]
    _KeyType = tuple[int,int,int,int]
    _IdxType = tuple[int,int,int,int,int]
    _AccessTupleType = tuple[int,int,int,int,int,int]

    def __init__(self,DB:DBInterface,**kwargs):
        TableName = 'F'
        IdxLabels = 's1,s2,U1,U2,W'
        ValueLabels = 'F,F_n'

        ReferenceName = 'lkp'
        KeyLabels = 'lL,kL,lR,kR'
        if kwargs is not None:
            kwargs["PartName"] = 'lk'
            kwargs["PartLabels"] = 'l,k'
        else:
            kwargs = { "PartName":'lk', "PartLabels":'l,k' }

        super().__init__(
            DB,
            TableName,KeyLabels,IdxLabels,ValueLabels,
            ReferenceName,
            **kwargs
        )
        self.ConfInst:FConf
        self.SourceTables:dict[sFConf,sFTable]

        self.SourceConf = {
            'Left':sFConf((self.ConfInst.RadialType,self.ConfInst.KTL)),
            'Right':sFConf((self.ConfInst.RadialType,self.ConfInst.KTR))
        }

    def _MakeIndex(self):

        super()._MakeIndex()

        subs = {
            'Named':'FAccess'+'_'+self.ConfInst.name,
            'Table':self.TableName,
            'Cols':'lL,kL,lR,kR,s1,s2,U1,U2,W,F'
        }
        comm = (
            "SELECT * FROM sqlite_schema WHERE type='index'"
            +" and name='{Named}'"
        ).format(**subs)
        if self.DB.Execute(comm).fetchone() is None:
            comm = (
                'CREATE INDEX IF NOT EXISTS {Named} ON {Table} ({Cols})'
            ).format(**subs)
            self.DB.Execute(comm)
            self.DB.Commit()

    @lru_cache
    def Access(self,tup:_AccessTupleType) -> Iterable[tuple[int,int,int,Expr]]:
        return tuple(
            (U1,U2,W,S(loads(F)))
            for (U1,U2,W,F) in
            self._MatchRows('U1,U2,W,F','lL,kL,lR,kR,s1,s2',(tup,))
        )

    def SourceInsert(self,Keys:Iterable[_KeyPartType]):
        Keys = tuple(Keys)
        self.SourceTables[self.SourceConf['Left']].Insert(Keys)
        self.SourceTables[self.SourceConf['Right']].Insert(Keys)

    @lru_cache
    def IdxTuple(self,key:_KeyType):
        return F_IdxTuple(key,self.ConfInst.KTL,self.ConfInst.KTR)

    def MapVal(self,Key:_KeyType,Idx:_IdxType):
        (lL,kL,lR,kR) = Key
        (s1,s2,U1,U2,W) = Idx

        tupL:sFTable._AccessTupleType = (lL,kL,s1,U1)
        tupR:sFTable._AccessTupleType = (lR,kR,s2,U2)

        x = (
            tupL,self.SourceConf["Left"],
            tupR,self.SourceConf["Right"]
        )
        res = (self.SubCalculation(*x))[W]
        return self.PackValues(res)

    @lru_cache
    def SubCalculation(
        self,
        tupL:sFTable._AccessTupleType,ConfL:sFConf,
        tupR:sFTable._AccessTupleType,ConfR:sFConf
    ) -> tuple[Expr,...]:

        U1 = tupL[-1]
        U2 = tupR[-1]

        ltL = self.sFAccess(self.SourceTables[ConfL],tupL)
        ltR = self.sFAccess(self.SourceTables[ConfR],tupR)

        return tuple(
            sum( ltL[x]*ltR[W-x] for x in range(max(0,W-U2),min(U1,W)+1) )
            for W in range(U1+U2+1)
        )

    @staticmethod
    @lru_cache
    def sFAccess(SourceTable:sFTable,tup:_AccessTupleType):
        return { x:sF for (x,sF) in SourceTable.Access(tup) }






# ==============================================================================

class GTable(CoeffTable):
    exp_t = Union[int,float]
    # keyPart_t = tuple[int,int]
    key_t = tuple[int,int,int,int]
    idx_t = tuple[int,int,int,int,int]
    accessTuple_t = tuple[int,int,int,int,int,int,int,int]

    def __init__(self,DB:DBInterface,**kwargs):
        TableName = 'G' # will need to be calculated from hash
        IdxLabels = 's1,s2,u,u1,u2,w'
        ValueLabels = 'G,G_n'

        ReferenceName = 'lkp'
        KeyLabels = 'lL,kL,lR,kR'
        if kwargs is not None:
            kwargs["PartName"] = 'lk'
            kwargs["PartLabels"] = 'l,k'
        else:
            kwargs = { "PartName":'lk', "PartLabels":'l,k' }

        super().__init__(
            DB,
            TableName,KeyLabels,IdxLabels,ValueLabels,
            ReferenceName,
            **kwargs
        )
        self.ConfInst:PairConf
        self.SourceTables:dict[FConf,FTable]
        

        self.SourceConf = FConf(
            (self.ConfInst.RadialType,self.ConfInst.TermType)
        )

    def _MakeIndex(self):

        super()._MakeIndex()

        subs = {
            'Named':'GAccess'+'_'+self.ConfInst.name,
            'Table':self.TableName,
            'Cols':'lL,kL,lR,kR,s1,s2,u,w,u1,G'
        }
        comm = (
            "SELECT * FROM sqlite_schema WHERE type='index'"
            +" and name='{Named}'"
        ).format(**subs)
        if self.DB.Execute(comm).fetchone() is None:
            comm = (
                'CREATE INDEX IF NOT EXISTS {Named} ON {Table} ({Cols})'
            ).format(**subs)
            self.DB.Execute(comm)
            self.DB.Commit()

        subs = {
            'Named':'GAccess1'+'_'+self.ConfInst.name,
            'Table':self.TableName,
            'Cols':'lL,kL,lR,kR,s1,s2,u1,u2,G'
        }
        comm = (
            "SELECT * FROM sqlite_schema WHERE type='index'"
            +" and name='{Named}'"
        ).format(**subs)
        if self.DB.Execute(comm).fetchone() is None:
            comm = (
                'CREATE INDEX IF NOT EXISTS {Named} ON {Table} ({Cols})'
            ).format(**subs)
            self.DB.Execute(comm)
            self.DB.Commit()

    @lru_cache
    def Access(self,tup:accessTuple_t) -> Iterable[tuple[int,Expr]]:
        return tuple(
            (u1,S(loads(G)))
            for (u1,G) in
            self._MatchRows(
                'u1,G','lL,kL,lR,kR,s1,s2,u,w',(tup,)
                )
        )

    @lru_cache
    def Access1(self,tup:accessTuple_t) -> Iterable[Expr]:
        return tuple(
            ( S(loads(G)), )
            for (G,) in
            self._MatchRows(
                'G','lL,kL,lR,kR,s1,s2,u1,u2',(tup,)
            )
        )

    def SourceInsert(self,Keys:key_t):
        self.SourceTables[self.SourceConf].Insert(Keys)

    @lru_cache
    def IdxTuple(self,key:key_t):
        return G_IdxTuple(key,self.ConfInst.KTL,self.ConfInst.KTR)

    def MapVal(self,key:key_t,idx:idx_t):
        (lL,kL,lR,kR) = key
        (s1,s2,_,u1,u2,w) = idx

        (uM1,uM2) = (
            uMax(kL,s1,self.ConfInst.KTL),uMax(kR,s2,self.ConfInst.KTR)
        )
        wM = uM1+uM2
        
        Wf:RadialType = self.ConfInst.RadialType
        Ex:Union[int,float] = self.ConfInst.Exp
        Kv:int = self.ConfInst.KVL + self.ConfInst.KVR
        pre = G_pre(symAvg(lL,lR),KD(s1,-1),KD(s2,-1),w,u1+u2,Wf,Ex,Kv)
        FAc = self.FAccess(
            self.SourceTables[self.SourceConf],
            (lL,kL,lR,kR,s1,s2)
        )
        res:self._ValType = pre * FAc[uM1-u1,uM2-u2,wM-w]

        return self.PackValues(res)
    
    @staticmethod
    @lru_cache
    def FAccess(SourceTable:FTable,tup:FTable._AccessTupleType):
        return { (U1,U2,W):F for (U1,U2,W,F) in SourceTable.Access(tup) }


from os import environ
Loc = environ.get("specPath")
from OperatorSpecification import GConf

class HITable(CoeffTable):
    exp_t = Union[int,float]
    # keyPart_t = tuple[int,int]
    key_t = tuple[int,int,int,int]
    idx_t = tuple[int,int,int,int]
    val_t = tuple[Expr,Expr]
    pval_t = tuple[bytes,float,bytes,float]
    accessTuple_t = tuple[int,int,int,int,int,int,int]
                    


    def __init__(self,DB:DBInterface,**kwargs):
        TableName = 'HI'
        IdxLabels = 's1,s2,u,w'
        ValueLabels = 'H,H_n,I,I_n'

        ReferenceName = 'lkp'
        KeyLabels = 'lL,kL,lR,kR'
        if kwargs is not None:
            kwargs["PartName"] = 'lk'
            kwargs["PartLabels"] = 'l,k'
        else:
            kwargs = { "PartName":'lk', "PartLabels":'l,k' }

        super().__init__(
            DB,
            TableName,
            KeyLabels,IdxLabels,ValueLabels,
            ReferenceName,
            **kwargs
        )
        self.ConfInst:PairConf
        self.SourceTables:dict[GConf,GTable]

        self.SourceConf = GConf(self.ConfInst)

    def _MakeIndex(self):

        super()._MakeIndex()

        subs = {
            'Named':'HIAccess'+'_'+self.ConfInst.name,
            'Table':self.TableName,
            'Cols':'lL,kL,lR,kR,s1,s2,u,w,H,H_n'
        }
        comm = (
            "SELECT * FROM sqlite_schema WHERE type='index'"
            +" and name='{Named}'"
        ).format(**subs)
        if self.DB.Execute(comm).fetchone() is None:
            comm = (
                'CREATE INDEX IF NOT EXISTS {Named} ON {Table} ({Cols})'
            ).format(**subs)
            self.DB.Execute(comm)
            self.DB.Commit()

        subs = {
            'Named':'HIAccess1'+'_'+self.ConfInst.name,
            'Table':self.TableName,
            'Cols':'lL,kL,lR,kR,s1,s2,u,H,I'
        }
        comm = (
            "SELECT * FROM sqlite_schema WHERE type='index'"
            +" and name='{Named}'"
        ).format(**subs)
        if self.DB.Execute(comm).fetchone() is None:
            comm = (
                'CREATE INDEX IF NOT EXISTS {Named} ON {Table} ({Cols})'
            ).format(**subs)
            self.DB.Execute(comm)
            self.DB.Commit()
    
    @lru_cache
    def Access(self,tup:accessTuple_t) -> Iterable[tuple[bytes,float]]:
        (lL,kL,lR,kR,s1,s2,w) = tup
        return tuple(self._MatchRows(
            'H,H_n',
            'lL,kL,lR,kR,s1,s2,u,w',
            ((lL,kL,lR,kR,s1,s2,0,w),)
        ))

    @lru_cache
    def Access1(self,tup:accessTuple_t) -> Iterable[tuple[Expr,Expr]]:
        return tuple(
            ( S(loads(H)), S(loads(I)) )
            for (H,I) in 
            self._MatchRows(
                'H,I',
                'lL,kL,lR,kR,s1,s2,u',
                (tup,)
            )   
        ) 

    @lru_cache
    def IdxTuple(self,key:key_t):
        return HI_IdxTuple(key,self.ConfInst.KTL,self.ConfInst.KTR)

    def MapVal(self,Key:key_t,Idx:idx_t):
        GAc = tuple(self.GAccess(self.SourceTables[self.SourceConf],Key+Idx))
        res0 = sum( G for (_,G,) in GAc)
        res1 = sum( (-1)**(u1)*G for (u1,G,) in GAc)
        return self.PackValues((res0,res1))

    @staticmethod
    @lru_cache
    def GAccess(SourceTable:GTable,tup:GTable._AccessTupleType):
        return tuple(SourceTable.Access(tup))

    @staticmethod
    def PackValues(Value:val_t) -> pval_t:
        res0 = CoeffTable.PackValues(Value[0])
        res1 = CoeffTable.PackValues(Value[1])
        return res0+res1

    @staticmethod
    def UnpackValues(pValue:pval_t) -> val_t:
        (pv0,_,pv1,_) = pValue
        return ( CoeffTable.UnpackValues(pv0), CoeffTable.UnpackValues(pv1) )


class JTable(CoeffTable):

    exp_t = Union[int,float]
    # keyPart_t = tuple[int,int]
    key_t = tuple[int,int,int,int]
    idx_t = tuple[int,int,int]
    accessTuple_t = tuple[int,int,int,int,int,int]

    def __init__(self,DB:DBInterface,**kwargs):
        TableName = 'J'
        IdxLabels = 's1,s2,w'
        ValueLabels = 'J,J_n'

        ReferenceName = 'lkp'
        KeyLabels = 'lL,kL,lR,kR'
        if kwargs is not None:
            kwargs["PartName"] = 'lk'
            kwargs["PartLabels"] = 'l,k'
        else:
            kwargs = { "PartName":'lk', "PartLabels":'l,k' }

        super().__init__(
            DB,
            TableName,
            KeyLabels,IdxLabels,ValueLabels,
            ReferenceName,
            **kwargs
        )
        self.ConfInst:GConf
        self.SourceTables:dict[GConf,JTable]

        self.SourceConf = GConf(self.ConfInst)

    def _MakeIndex(self):

        super()._MakeIndex()

        subs = {
            'Named':'JAccess'+'_'+self.ConfInst.name,
            'Table':self.TableName,
            'Cols':'lL,kL,lR,kR,s1,s2,J'
        }
        comm = (
            "SELECT * FROM sqlite_schema WHERE type='index'"
            +" and name='{Named}'"
        ).format(**subs)
        if self.DB.Execute(comm).fetchone() is None:
            comm = (
                'CREATE INDEX IF NOT EXISTS {Named} ON {Table} ({Cols})'
            ).format(**subs)
            self.DB.Execute(comm)
            self.DB.Commit()

    @lru_cache
    def Access1(self,tup:accessTuple_t) -> tuple[Expr]:
        return tuple(
            ( S(loads(J)), )
            for (J,) in
            self._MatchRows(
                'J',
                'lL,kL,lR,kR,s1,s2',
                (tup,)
            )
        )

    @lru_cache
    def IdxTuple(self,key:key_t):
        return J_IdxTuple(key,self.ConfInst.KTL,self.ConfInst.KTR)

    def MapVal(self,key:key_t,idx:idx_t):
        return self.HIAccess(self.SourceTables[self.SourceConf],key+idx)

    @staticmethod
    @lru_cache
    def HIAccess(SourceTable:HITable,tup:HITable.accessTuple_t):
        return SourceTable.Access(tup)[0]


class gTable(CoeffTable):
    # keyPart_t = tuple[int,int]
    key_t = tuple[int,int,int,int]
    idx_t = tuple[int,int,int,int,int]

    def __init__(self,DB:DBInterface,**kwargs):
        TableName = 'GContracted'
        IdxLabels = 's1,s2,u,u1,u2'
        ValueLabels = 'g,g_n'

        ReferenceName = 'lkp'
        KeyLabels = 'lL,kL,lR,kR'
        if kwargs is not None:
            kwargs["PartName"] = 'lk'
            kwargs["PartLabels"] = 'l,k'
        else:
            kwargs = { "PartName":'lk', "PartLabels":'l,k' }

        super().__init__(
            DB,
            TableName,
            KeyLabels,IdxLabels,ValueLabels,
            ReferenceName,
            **kwargs
        )
        self.ConfInst:GConf
        self.SourceTables:dict[GConf,GTable]

        self.SourceConf = GConf(self.ConfInst)

    @lru_cache
    def IdxTuple(self,key:key_t):
        return g_IdxTuple(key,self.ConfInst.KTL,self.ConfInst.KTR)

    def MapVal(self,Key:key_t,Idx:idx_t):
        (s1,s2,_,u1,u2) = Idx
        tup = Key+(s1,s2,u1,u2)
        GAc = self.GAccess(self.SourceTables[self.SourceConf],tup)
        return self.PackValues(sum( G for (G,) in GAc ))

    @staticmethod
    @lru_cache
    def GAccess(SourceTable:GTable,tup:GTable.accessTuple_t):
        return tuple(SourceTable.Access1(tup))


class hiTable(CoeffTable):
    # keyPart_t = tuple[int,int]
    key_t = tuple[int,int,int,int]
    idx_t = tuple[int,int,int]
    val_t = tuple[Expr,Expr]
    pval_t = tuple[bytes,float,bytes,float]

    def __init__(self,DB:DBInterface,**kwargs):
        TableName = 'HIContracted'
        IdxLabels = 's1,s2,u'
        ValueLabels = 'h,h_n,i,i_n'

        ReferenceName = 'lkp'
        KeyLabels = 'lL,kL,lR,kR'
        if kwargs is not None:
            kwargs["PartName"] = 'lk'
            kwargs["PartLabels"] = 'l,k'
        else:
            kwargs = { "PartName":'lk', "PartLabels":'l,k' }

        super().__init__(
            DB,
            TableName,
            KeyLabels,IdxLabels,ValueLabels,
            ReferenceName,
            **kwargs
        )
        self.ConfInst:GConf
        self.SourceTables:dict[GConf,HITable]

        self.SourceConf = GConf(self.ConfInst)

    @lru_cache
    def IdxTuple(self,key:key_t):
        return hi_IdxTuple(key,self.ConfInst.KTL,self.ConfInst.KTR)

    def MapVal(self,Key:key_t,Idx:idx_t):
        tup = Key+Idx
        HIAc = self.HIAccess(self.SourceTables[self.SourceConf],tup)
        res0 = sum( H for (H,_,) in HIAc )
        res1 = sum( I for (_,I,) in HIAc )
        return self.PackValues( (res0,res1) )

    @staticmethod
    @lru_cache
    def HIAccess(SourceTable:HITable,tup:HITable.accessTuple_t):
        return tuple(SourceTable.Access1(tup))

    @staticmethod
    def PackValues(Value:val_t) -> pval_t:
        res0 = CoeffTable.PackValues(Value[0])
        res1 = CoeffTable.PackValues(Value[1])
        return res0+res1

    @staticmethod
    def UnpackValues(pValue:pval_t) -> val_t:
        (pv0,_,pv1,_) = pValue
        return ( CoeffTable.UnpackValues(pv0), CoeffTable.UnpackValues(pv1) )


class jTable(CoeffTable):
    # keyPart_t = tuple[int,int]
    key_t = tuple[int,int,int,int]
    idx_t = tuple[int,int]

    def __init__(self,DB:DBInterface,**kwargs):
        TableName = 'JContracted'
        IdxLabels = 's1,s2'
        ValueLabels = 'j,jn'

        ReferenceName = 'lkp'
        KeyLabels = 'lL,kL,lR,kR'
        if kwargs is not None:
            kwargs["PartName"] = 'lk'
            kwargs["PartLabels"] = 'l,k'
        else:
            kwargs = { "PartName":'lk', "PartLabels":'l,k' }

        super().__init__(
            DB,
            TableName,
            KeyLabels,IdxLabels,ValueLabels,
            ReferenceName,
            **kwargs
        )
        self.ConfInst:GConf
        self.SourceTables:dict[GConf,JTable]

        self.SourceConf = GConf(self.ConfInst)

    @lru_cache
    def IdxTuple(self,key:key_t):
        (l1,_,l2,_) = key
        return j_IdxTuple(l1,l2,self.ConfInst.KTL,self.ConfInst.KTR)

    def MapVal(self,Key:key_t,Idx:idx_t):
        tup = Key+Idx
        JAc = self.JAccess(self.SourceTables[self.SourceConf],tup)
        return self.PackValues(sum( J for (J,) in JAc ))

    @staticmethod
    @lru_cache
    def JAccess(SourceTable:JTable,tup:JTable.accessTuple_t):
        return tuple(SourceTable.Access1(tup))









class CoeffManager:
    TableClass:CoeffTable = None
    SourceClass:CoeffManager = None

    def __init__(
        self,
        DB:DBInterface
    ):

        self.DB = DB
        self.TableType = self.TableClass.__name__

        if self.SourceClass is not None:
            self.InstantiateSource()
        self.InstantiateTables()

    @staticmethod
    @lru_cache
    def ParseConf(c):
        return c

    def InstantiateSource(self):
        self.Source = self.SourceClass(self.DB)
        self.SourceTables = self.Source.Tables

    def InstantiateTables(self):
        if self.Conf is not None:
            self._TablesType = dict[self.Conf,self.TableClass]
            if self.SourceClass is not None:
                self.Tables:self._TablesType = {
                    c:self.TableClass(
                        self.DB,
                        ConfInst=c,
                        SourceTables=self.SourceTables
                    ) for c in self.Conf
                }
            else:
                self.Tables:self._TablesType = {
                    c:self.TableClass(self.DB,c)
                    for c in self.Conf
                }
        else:
            self.table_t = self.TableClass
            self.Tables:self.table_t = self.TableClass(self.DB)

    def Insert(self,*args):
        Keys = tuple(args[0])
        try:
            c:self.Conf = args[1]
            self.Tables[c].Insert(Keys)
        except IndexError:
            if self.Conf is not None:
                for c in self.Conf:
                    self.Tables[c].Insert(Keys)
            else:
                self.Tables.Insert(Keys)


class sF(CoeffManager):
    Conf = sFConf
    TableClass = sFTable

class F(CoeffManager):
    Conf = FConf
    TableClass = FTable
    SourceClass = sF

class G(CoeffManager):
    Conf = GConf
    TableClass = GTable
    SourceClass = F

class HI(CoeffManager):
    Conf = GConf
    TableClass = HITable
    SourceClass = G    

class J(CoeffManager):
    Conf = GConf
    TableClass = JTable
    SourceClass = HI

class g(CoeffManager):
    Conf = GConf
    TableClass = gTable
    SourceClass = G

class hi(CoeffManager):
    Conf = GConf
    TableClass = hiTable
    SourceClass = HI

class j(CoeffManager):
    Conf = GConf
    TableClass = jTable
    SourceClass = J