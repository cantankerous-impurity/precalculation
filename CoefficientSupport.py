from enum import Enum
# from enum import EnumMeta
from itertools import product,chain
from itertools import combinations_with_replacement as cwr
from functools import lru_cache
from pickle import dumps, loads
from typing import Union

from sympy import S,Expr
from sympy import Rational,binomial,ff,sqrt as ssqrt
from sympy.functions.special.gamma_functions import gamma

from uuid import uuid4

from sys import path as syspath
from os import environ
syspath.append(environ.get("coeffPath"))
from DatabaseInterface import DBInterface, KeyValTable
Loc = environ.get("precDataDir")
OpDB = DBInterface("Operators.db",Loc)
CoeffsDB = DBInterface("Coeffs.db",Loc)




class RadialType(Enum):
    Slater = 1
    Gauss = 2

class KinType(Enum):
    Ovr = False
    Kin = True

class TermType(Enum):
    _ignore_ = "val_t"
    val_t = tuple[KinType,Union[int,float],KinType]
    II = (KinType.Ovr,KinType.Ovr)
    IK = (KinType.Ovr,KinType.Kin)
    KI = (KinType.Kin,KinType.Ovr)
    KK = (KinType.Kin,KinType.Kin)



class sFConf(Enum):
    _ignore_ = "val_t pval_t"
    val_t = tuple[RadialType,KinType]
    pval_t = tuple[int,bool]

    Slater_I = (RadialType.Slater,KinType.Ovr)
    Slater_K = (RadialType.Slater,KinType.Kin)
    Gauss_I = (RadialType.Gauss,KinType.Ovr)
    Gauss_K = (RadialType.Gauss,KinType.Kin)

    def __init__(self,rT:RadialType,kT:KinType):
        self.Wf:RadialType = rT
        self.RadialType:RadialType = rT
        self.KinType:KinType = kT
        self.RadialValue:int = self.RadialType.value
        self.KinValue:bool = self.KinType.value

    def Values(self) -> val_t:
        return (self.RadialType,self.KinType)

    def Packed(self) -> pval_t:
        return (self.RadialValue,self.KinValue)



class PairConf(Enum):
    
    def __init__(self,rT:RadialType,tT:TermType):
        self.RadialType:RadialType = rT
        self.TermType:TermType = tT
        self.KinTypeLeft:KinType = (tT.value)[0]
        self.KinTypeRight:KinType = (tT.value)[1]

        self.RadialValue = self.RadialType.value
        self.KinValueLeft = self.KinTypeLeft.value
        self.KinValueRight = self.KinTypeRight.value

        self.RT = self.RadialType
        self.KTL = self.KinTypeLeft
        self.KTR = self.KinTypeRight

        self.Wf = self.RadialValue
        self.KVL = self.KinValueLeft
        self.KVR = self.KinValueRight

class FConf(PairConf):
    _ignore_ = "val_t pval_t"
    val_t = tuple[RadialType,TermType]
    pval_t = tuple[int,bool,bool]

    Slater_II = (RadialType.Slater,TermType.II)
    Slater_IK = (RadialType.Slater,TermType.IK)
    Slater_KI = (RadialType.Slater,TermType.KI)
    Slater_KK = (RadialType.Slater,TermType.KK)
    Gauss_II = (RadialType.Gauss,TermType.II)
    Gauss_IK = (RadialType.Gauss,TermType.IK)
    Gauss_KI = (RadialType.Gauss,TermType.KI)
    Gauss_KK = (RadialType.Gauss,TermType.KK)




class OperatorIds(KeyValTable):
    def __init__(self,DB:DBInterface):
        super().__init__(DB,"OperatorIds","exponent","uuid")

    def GetID(self,exponent):
        row = tuple(super()._MatchRows("uuid","exponent",( (exponent,),)))
        return str(row[0][0])

    def FindNewRows(self,Rows):
        Rows = tuple(sorted(set(Rows)))
        return super().FindNewRows(Rows)

    @staticmethod
    def UnpackValues(V):
        return V

    @staticmethod
    def PackValues(v):
        return v

    @staticmethod
    def MapVal(x):
        try:
            if len(x) == 2:
                x = (x[1],)
        except TypeError:
            pass
        try:
            if int(x) == x:
                res = str(abs(x))
                if x < 0:
                    res = 'm'+res
                return (res,)
        except (ValueError,TypeError):
            pass
        return (uuid4().hex,)


class GConfBase(PairConf):
    _ignore_ = "val_t pval_t"
    val_t = tuple[RadialType,TermType,Union[int,float]]
    pval_t = tuple[int,bool,bool,Union[int,float]]

    def __new__(cls,*args):
        member = object.__new__(cls)
        member._value_ = args
        return member

    def __init__(self,*record):
        super().__init__(*record[0:-1])
        self.exp = record[-1]
        self.Exp = self.exp


def MakeGConf(ExpSpec):
    ExpSpec = tuple(ExpSpec) + ( 0, )
    OperatorTable = OperatorIds(OpDB)
    OperatorTable.Insert(chain(
        ( (x,) for x in  ExpSpec ),
        set( (x0+x1,) for x0,x1 in  cwr(ExpSpec,2) )
    ))

    def GetGConfOps():
        res = []
        for rT in RadialType:
            for exp in set(ExpSpec):
                for ops in (
                    (TermType.II,exp),
                    (TermType.IK,exp),(TermType.KI,exp),
                ):
                    (tT,d) = ops
                    OPID = OperatorTable.GetID(d)
                    _name = rT.name+"_"+tT.name+"_"+OPID
                    _value = (rT,tT,d)
                    x = (_name,_value)
                    if x not in res:
                        res.append(x)
            for exp in set(x0+x1 for x0,x1 in cwr(ExpSpec,2)):
                for ops in (
                    (TermType.II,exp),
                ):
                    (tT,d) = ops
                    OPID = OperatorTable.GetID(d)
                    _name = rT.name+"_"+tT.name+"_"+OPID
                    _value = (rT,tT,d)
                    x = (_name,_value)
                    if x not in res:
                        res.append(x)
        return tuple(sorted(set(
            chain( 
                (
                    ( "Slater_II_0" , (RadialType.Slater,TermType.II,0) ),
                    ( "Slater_IK_0" , (RadialType.Slater,TermType.IK,0) ),
                    ( "Slater_KI_0" , (RadialType.Slater,TermType.KI,0) ),
                    ( "Slater_KK_0" , (RadialType.Slater,TermType.KK,0) ),

                    ( "Gauss_II_0" , (RadialType.Gauss,TermType.II,0) ),
                    ( "Gauss_IK_0" , (RadialType.Gauss,TermType.IK,0) ),
                    ( "Gauss_KI_0" , (RadialType.Gauss,TermType.KI,0) ),
                    ( "Gauss_KK_0" , (RadialType.Gauss,TermType.KK,0) ),
                ),
                res,
            )
        )))

    return GConfBase('GConf',GetGConfOps())


    
@lru_cache
def PackExpr(Value:Expr) -> tuple[bytes,float]:
    return ( dumps(Value), float(Value), )


@lru_cache
def UnpackExpr(pValue:tuple[bytes,float]) -> Expr:
    return S(loads(pValue[0]))






@lru_cache
def KD(x1,x2):
    return 1 if x1 == x2 else 0

@lru_cache
def Binom(n,m):
    return binomial(n,m)

@lru_cache
def Gam(x):
    return gamma(x)

@lru_cache
def AlphaF(Wf:RadialType,l):
    return Rational(2,Wf.value)*(l + Rational(1,Wf.value))

@lru_cache
def FallAlk(Wf:RadialType,l,k,x):
    return ff(AlphaF(Wf,l)+k,x)



@lru_cache
def sD_C(m,l,k,s):
    def Func(l,m,s):
        N = ( (l+s)**2 - m**2 ) * ( (l+1+s)**2 - m**2 )
        D = (2*l+2*s-1)*(2*l+2*s+3)*(2*l+2*s+1)**2
        return ssqrt(Rational(N,D))
    if s == 0:
        R = Rational(2*l**2 - 2*m**2 + 2*l - 1,(2*l-1)*(2*l+3))
    else:
        R = Func(l,m,s)

    def Denominator(k,s):
        p = (1-s)//2
        return (k+1)*(1+p*(k+1))
    return R/Denominator(k,s)

@lru_cache
def CsF(Wf:RadialType,l,k):
    return 1/ssqrt(gamma(k+1)*gamma(AlphaF(Wf,l)+k+1))

@lru_cache
def BsF(K,U,x):
    return Binom(K,U)*Binom(U,x)

@lru_cache
def F0sF(Wf:RadialType,l,k,x):
    return CsF(Wf,l,k)*FallAlk(Wf,l,k,x)

@lru_cache
def HKsF(Wf:RadialType,l,k,s,x):
    a = AlphaF(Wf,l) + k
    kd1 = KD(Wf.value,1)
    kd2 = KD(Wf.value,2)
    if s == +1:
        res = k-x
        res += Rational(2*x*(2*x*(k+kd2-x)+kd1),a+1-x)
        res += Rational(4*x*(-k+x-kd2)*(x-1),a+2-x)
        return res
    if s == 0:
        res = k+x
        res += Rational(2*x*(k-kd1*(x-1)+kd2*x),a+1-x)
        res += kd1*Rational(4*(x-1)*x,a+2-x)
        return res
    if s == -1:
        res = -(k+1+x)*(k+2+x)+2*x-1
        res += kd1*(Rational(6*x*(k+2-x),a+1-x) + Rational(12*(x-1)*x,a+2-x))
        return res


@lru_cache
def sF_C(Wf:RadialType,l,k,U,x):
    return F0sF(Wf,l,k,x)*BsF(k,U,x)

@lru_cache
def sF_Kin_C(Wf:RadialType,l,k,s,U,x):
    return F0sF(Wf,l,k,x)*BsF(k+1+KD(s,-1),U,x)*(1+HKsF(Wf,l,k,s,x))









@lru_cache
def sC(Wf:RadialType,Ex):
    return ( Rational(Wf.value,2)**Ex, Rational(Ex,Wf.value) )

@lru_cache
def symAvg(x1,x2):
    return Rational(x1+x2,2)

@lru_cache
def gM(Lv,kd1,kd2,w,Wf:RadialType,Ex,Kn):
    z = AlphaF(Wf,Lv) + w + sC(Wf,Ex)[1]
    z -= KD(Wf.value,1)*Kn + kd1 + kd2
    return gamma(z+1)

@lru_cache
def G_pre(Lv:Rational,kd1:int,kd2:int,w:int,u:int,Wf:RadialType,Ex,Kn:int):
    return (-1)**(w+u) * sC(Wf,Ex)[0] * gM(Lv,kd1,kd2,w,Wf,Ex,Kn)










@lru_cache
def uMax(k:int,s:int,K:KinType) -> int:
    return k+K.value+KD(s,-1)


@lru_cache
def sTuple(l:int,K:KinType):
    Kv = K.value
    return tuple( s for s in range(-min(Kv,l//2),Kv+1))
@lru_cache
def suTuple(l:int,k:int,K:KinType):
    return tuple(  (s,u) for s in sTuple(l,K) for u in range(uMax(k,s,K)+1) )
@lru_cache
def wTuple(wMin:int,wMax:int):
    return tuple(range(wMin,wMax+1))
@lru_cache
def uwTuple(wM:int):
    return tuple( (u,w) for u in wTuple(0,wM) for w in wTuple(u,wM) )
@lru_cache
def u12Tuple(u:int,uM1:int,uM2:int):
    return tuple( (u1,u - u1) for u1 in range(max(u-uM2,0),min(u,uM1)+1) )


@lru_cache
def uu12wTuple(uM1:int,uM2:int):
    return tuple( 
        (u,u1,u2,w)
        for u,w in uwTuple(uM1+uM2)
        for u1,u2 in u12Tuple(u,uM1,uM2)
    )








def sF_IdxGen(Key,Kin:KinType):
    (l,k) = tuple[int,int](Key)
    for s,U in suTuple(l,k,Kin):
        for x in range(U+1):
            yield (s,U,x)

def G_IdxGen(Key:tuple[int,int,int,int],Kin1:KinType,Kin2:KinType):
    (l1,k1,l2,k2) = Key
    for s1,s2, in product(sTuple(l1,Kin1),sTuple(l2,Kin2)):
        for u,u1,u2,w in uu12wTuple(uMax(k1,s1,Kin1),uMax(k2,s2,Kin2)):
            yield (s1,s2,u,u1,u2,w)

def F_IdxGen(Key:tuple[int,int,int,int],Kin1:KinType,Kin2:KinType):
    (_,k1,_,k2) = Key
    for s1,s2,_,u1,u2,w in G_IdxTuple(Key,Kin1,Kin2):
        (uM1,uM2) = (uMax(k1,s1,Kin1),uMax(k2,s2,Kin2))
        wM = uM1+uM2
        (u1,u2,w) = (uM1-u1,uM2-u2,wM-w)
        yield (s1,s2,u1,u2,w)

def HI_IdxGen(Key:tuple[int,int,int,int],Kin1:KinType,Kin2:KinType):
    (l1,k1,l2,k2) = tuple[int,int,int,int](Key)
    for s1,s2, in product(sTuple(l1,Kin1),sTuple(l2,Kin2)):
        for u,w in uwTuple(uMax(k1,s1,Kin1)+uMax(k2,s2,Kin2)):
            yield (s1,s2,u,w)

def J_IdxGen(Key:tuple[int,int,int,int],Kin1:KinType,Kin2:KinType):
    (l1,k1,l2,k2) = tuple[int,int,int,int](Key)
    for s1,s2, in product(sTuple(l1,Kin1),sTuple(l2,Kin2)):
        for w in wTuple(0,uMax(k1,s1,Kin1)+uMax(k2,s2,Kin2)):
            yield (s1,s2,w)

def g_IdxGen(Key,Kin1:KinType,Kin2:KinType):
    (l1,k1,l2,k2) = tuple[int,int,int,int](Key)
    for s1,s2, in product(sTuple(l1,Kin1),sTuple(l2,Kin2)):
        (uM1, uM2) = (uMax(k1,s1,Kin1),uMax(k2,s2,Kin2))
        for u in wTuple(0,uM1+uM2):
            for u1,u2 in u12Tuple(u,uM1,uM2):
                yield (s1,s2,u,u1,u2)

def hi_IdxGen(Key,Kin1:KinType,Kin2:KinType):
    (l1,k1,l2,k2) = tuple[int,int,int,int](Key)
    for s1,s2, in product(sTuple(l1,Kin1),sTuple(l2,Kin2)):
        for u in wTuple(0,uMax(k1,s1,Kin1)+uMax(k2,s2,Kin2)):
            yield (s1,s2,u)

def j_IdxGen(l1:int,l2:int,Kin1:KinType,Kin2:KinType):
    for s1,s2, in product(sTuple(l1,Kin1),sTuple(l2,Kin2)):
        yield (s1,s2)







@lru_cache
def sF_IdxTuple(Key:tuple[int,int],Kin:KinType):
    return tuple(sF_IdxGen(Key,Kin))

@lru_cache
def G_IdxTuple(Key:tuple[int,int,int,int],Kin1:KinType,Kin2:KinType):
    return tuple( G_IdxGen(Key,Kin1,Kin2) )

@lru_cache
def F_IdxTuple(Key:tuple[int,int,int,int],KinL:KinType,KinR:KinType):
    return tuple( F_IdxGen(Key,KinL,KinR) )

@lru_cache
def HI_IdxTuple(Key:tuple[int,int,int,int],Kin1:KinType,Kin2:KinType):
    return tuple( HI_IdxGen(Key,Kin1,Kin2) )

@lru_cache
def J_IdxTuple(Key:tuple[int,int,int,int],Kin1:KinType,Kin2:KinType):
    return tuple( J_IdxGen(Key,Kin1,Kin2) )

@lru_cache
def g_IdxTuple(Key:tuple[int,int,int,int],Kin1:KinType,Kin2:KinType):
    return tuple( g_IdxGen(Key,Kin1,Kin2) )

@lru_cache
def hi_IdxTuple(Key:tuple[int,int,int,int],Kin1:KinType,Kin2:KinType):
    return tuple( hi_IdxGen(Key,Kin1,Kin2) )

@lru_cache
def j_IdxTuple(l1:int,l2:int,Kin1:KinType,Kin2:KinType):
    return tuple( j_IdxGen(l1,l2,Kin1,Kin2) )