# cantakerous-impurity-precalculation
Precalculation is a subproject under the umbrella of Cantankerous Impurity. This subproject is dedicated to handling the issues involving with computation of the precalculated coefficients used by the matrix generation formulae. 

We model our silicon impurity states as a superposition of single-particle basis states constructed from the product of a Slater or Gaussian radial decay function, a radial orthogonal polynomial, and the spherical harmonics. From this, we can elaborate formuale and methods for calculation of the matrix elements of a reasonably broad class of Hamiltonians. Due to our use of orthogonal polynomials to construct basis states, the matrix construction formulae require, in general, calculation of many real coefficients. Additionally, computations of these coefficients involve potentially ill-conditioned sums of alternating sequences of real numbers. 

Precalculation exists to address two main goals.

1. Given a pair of indices completely specifying the form of a pair of given basis functions, efficiently calculate the needed coefficients without risk of uncontrolled error.
2. Leverage some data IO and storage solution to ensure the results of already completed computations are preserved to be used for downstream coefficient computations, such as for growth of the basis .

To prevent the possibility of runaway error due to these alternating sums raised in Goal #1 we have employed symbolic calculations using SymPy, a Python package for symbolic math. This effectively sidesteps the consequences of floating point subtraction.

To accomplish Goal #2, we save and retrieve needed cofficients from a set of SQLite3 databases, managed by Python's sqlite3 package.

# cantankerous-impurity
Cantankerous Impurity is an attempt to completely and adequately describe the spectrum and wavefunction of silicon donors / point-like impurities by extending and completing the effective mass model.

Silicon crystals can be host to impurities that attract excess electrons. If the impurity potential is weak enough, we expect that the electron's state will be localized near conduction band minima. Through a few simple and somewhat persuasive approximations, we can smear out the silicon background. As such, we model the electron's interactions with the surrounding media and the impurity using an effective Hamiltonian. We expect this Hamiltonian to contain a kinetic energy-like term with a modified effective mass as well as a potential term intended to effectively capture all deviations from the pure Silicon crystal potential.

It is unclear to what extent an isolated donor in a silicon crystal can be said to defy the effective mass approximation (EMA). As such, it is unclear what is lost in qualitative and descriptive power when employing the EMA. This is complicated by the fact that existing effective mass models often contain unquantified amounts of truncation and approximation error. It is not clear how much error comes from physical approximations (such as assuming that the electron wavefunction is strongly localized within k-space at conduction band minima) and how much error comes from mathematical approximations (such as those introduced by using "approximate" wavefunctions such as variational wavefunctions or truncated bases)

In Cantankerous Impurity we seek to accomplish two primary goals:
1) Quantify to what degree a particular wavefunction can be said to "solve" the implicit problem posed by an effective mass model.
2) Explore substantial additions and corrections to the effective mass model. In particular, we wish to consider how to model the exchange and correlation effects of the core valence electrons of silicon atoms close to the donor.

